#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml
ansible-playbook L3.create-app.yml \
                -i inventories/servers/hosts \
                "$@"