#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml
ansible-playbook deploy-all.yml \
                -i inventories/servers/hosts \
                --ask-vault-pass \
                "$@"

