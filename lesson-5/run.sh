#!/usr/bin/env bash

pip install -r requirements.txt
ansible-galaxy install -r requirements.yml
ansible-playbook deploy-all.yml \
                --ask-vault-pass \
                "$@"

