#!/usr/bin/env bash

ansible-playbook L2.create-file.yml \
                -i inventories/servers/hosts \
                "$@"